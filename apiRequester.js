axios = require('axios');

const translateRequester = function (inputLanguage, targetLanguage, text){
    var strResponse ='';
    axios.get('http://translate.googleapis.com/translate_a/single',{
        params: {
            client: 'gtx',
            sl: inputLanguage,
            tl: targetLanguage,
            dt: 't',
            q: text
        },
    })
    .then(function (response) {
        if(response == undefined){
            return '';
        }
        response.data[0].forEach(element => {
            strResponse = strResponse+element[0]
        });
        console.log(strResponse);
    })
    .catch(function (error) {
        console.error(error);
    });
}
module.exports = {
    requester: translateRequester,
}
//https://translate.googleapis.com/translate_a/single?client=gtx&sl={{Langue de originale}}&tl={{Langue destination}}&dt=t&q={{Message a traduire}}