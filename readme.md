# Mise en place du projet

## Pré-requis
- node
- npm

## Installation
### Importer le code du projet
```git clone https://gitlab.com/Flavien_Louis/chat_bot_translate.git```
### Installer les dépendances
``` npm i```
### Lancer le projet
``` node index.js```