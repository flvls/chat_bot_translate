const readline 		= require('readline');
const translateModule = require('./apiRequester');
const getLang = require('./findLang');

var sourceLang = '';
var sourceLangFullFrName = '';

var targetLang = '';
var targetLangFullFrName = '';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.write("Bonjour bienvenu sur le bot de traduction !");
rl.clearLine();

getInfo();

function getInfo(){
    rl.question("Quelle langue parlez vous ?", (sourceLangAnswer) => {
        sourceLangInfo = getLang.getLangModule(sourceLangAnswer);
        sourceLang = sourceLangInfo.lang;
        sourceLangFullFrName = sourceLangInfo.langFullFrName;
        rl.question(translateModule.requester('fr', sourceLang,'Oh vous parlez '+sourceLangFullFrName + ' En quelle langue voulez vous traduire ?'), (targetLangAnswer) => {
            targetLangInfo = getLang.getLangModule(targetLangAnswer);
            targetLang = targetLangInfo.lang;
            targetLangFullFrName = targetLangInfo.langFullFrName;
            rl.write(translateModule.requester('fr', sourceLang, 'Le texte que vous entrez sera traduit en '+targetLangFullFrName));
        });
    });
}

function getSourceLang(){
    rl.question("Quelle langue parlez vous ?", (sourceLangAnswer) => {
        rl.clearLine();
        sourceLangInfo = getLang.getLangModule(sourceLangAnswer);
        sourceLang = sourceLangInfo.lang;
        sourceLangFullFrName = sourceLangInfo.langFullFrName;

        rl.write(translateModule.requester('fr', sourceLang, 'Oh vous parlez '+sourceLangFullFrName, 'Le texte que vous entrez sera traduit en '+targetLangFullFrName));
        rl.clearLine();
    });

}
function getTargetLang(){
    rl.question(translateModule.requester('fr', sourceLang, 'En quelle langue voulez vous traduire ?'), (targetLangAnswer) => {
        targetLangInfo = getLang.getLangModule(targetLangAnswer);
        targetLang = targetLangInfo.lang;
        targetLangFullFrName = targetLangInfo.langFullFrName;
        rl.write(translateModule.requester('fr', sourceLang, 'Le texte que vous entrez sera traduit en '+targetLangFullFrName));
        rl.clearLine();
    });
}

const commands = [
	{ 'trigger' : '!source',
	  'output'  : getSourceLang
	},
	{ 'trigger' : '!target',
	  'output'  : getTargetLang
    },
    { 'trigger' : '!reset',
	  'output'  : getInfo
	}
];

rl.on('line', function(input){
    if (input.charAt(0) == "!"){
        let found = false;
        commands.forEach(command => {
            if (input.match(command.trigger) != null && !found){
                found = true;
                command.output();
            }
        });
    }else {
        rl.write(translateModule.requester(sourceLang, targetLang, input));
        rl.clearLine();    
    }
});
