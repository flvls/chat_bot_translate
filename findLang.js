let fr = require('./languages/fr');
let en = require('./languages/en');
let de = require('./languages/de');

const getLang = function getLang(fullLang){
    if (isFromThisCountry(fr, fullLang)){
        return {lang: 'fr', langFullFrName: 'Français'};
    }
    if (isFromThisCountry(en, fullLang)){
        return {lang: 'en', langFullFrName: 'Anglais'};
    }
    if (isFromThisCountry(de, fullLang)){
        return {lang: 'de', langFullFrName: 'Allemand'};
    }
}

let isFromThisCountry = function isFromThisCountry(arrayNames, fullLang){
    let hasMatch = false;
    arrayNames.lang.forEach(fullName => {
        if (fullName == fullLang){
            hasMatch = true;
        }
    });
    return hasMatch;
}

module.exports = {
    getLangModule: getLang
}