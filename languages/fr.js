let fr = [
    //Orthographe fr
    'Français',
    'français',
    'france',
    'fr',
    'francais',
    'franssai',
    'françé',
    //Orthographe anglaise
    'France',
    'French',
    'french',
    //Orthographe allemende
    'französisch',
    'Französisch',
    'Franzosisch',
    'franzosisch',
    //Orthographe Espagnol
    'francés',
    'Francés',
    'francs',
]

module.exports = {
    lang: fr,
}