let de = [
    //Orthographe fr
    'allemand',
    'Allemand',
    'alleman',
    'Alleman',
    'allemend',
    'Allemend',
    'Alemand',
    'alemand',
    //Orthographe anglaise
    'German',
    'german',
    'germann',
    'Germann',
    //Orthographe allemende
    'Deutsh',
    'deutsh',
    'Deutshe',
    'deutshe',
    //Orthographe Espagnol
    'Aleman',
    'aleman',
    'de',
]

module.exports = {
    lang: de,
}