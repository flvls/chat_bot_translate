let en = [
    //Orthographe fr
    'Anglais',
    'anglais',
    'anglé',
    'englé',
    'anglai',
    'Anglai',
    'Englais',
    'Englais',
    //Orthographe anglaise
    'English',
    'english',
    'Anglish',
    'anglish',
    //Orthographe allemende
    'englisch',
    'Englisch',
    'Anglisch',
    'anglisch',
    //Orthographe Espagnol
    'Inglés',
    'inglés',
    'Ingles',
    'ingles',
    'Ingle',
    'ingle',
    'en',
]

module.exports = {
    lang: en,
}